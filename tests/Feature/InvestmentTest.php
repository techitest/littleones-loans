<?php

namespace Tests\Feature;

use App\Exceptions\GeneralException;
use App\Models\Investment;
use App\Models\Investor;
use App\Models\Loan;
use App\Models\Tranche;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InvestmentTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->startDate = '01/10/2020';
        $this->endDate = '15/11/2020';

        $this->loan = new Loan($this->startDate, $this->endDate);
        $this->trancheA = new Tranche('Tranche A',3, 1000);
        $this->trancheB = new Tranche('Tranche B',6, 1000);

        $this->loan->addTranch($this->trancheA);
        $this->loan->addTranch($this->trancheB);
    }

    /** @test */
    public function investor1_to_invest_1000_in_trancheA()
    {
        $investor1 = new Investor('Investor1');
        $investmentDate = '03/10/2020';
        $investment = new Investment($investor1, $this->trancheA, '1000', $investmentDate);

        $this->loan->addLoanInvestment($investment);

        $this->assertTrue($this->loan->doesLoanHasInvestment($investment));
    }

    /** @test */
    public function loan_can_only_receive_investments_on_existing_tranches()
    {
        try {
            // Investor1 investment
            $investor1 = new Investor('Investor1');
            $investmentDate = '03/10/2020';
            $trancheC = new Tranche('Tranche C',7, 1000);

            $investment = new Investment($investor1, $trancheC, '1000', $investmentDate);

            $this->loan->addLoanInvestment($investment);

        } catch (GeneralException $ex) {
            $this->assertSame('This Tranche does not exist on this Loan', $ex->getMessage());
        }

    }

    /** @test */
    public function investor2_invests_100_after_investor1_on_trancheA()
    {
        try {
            // Investor1 investment
            $investor1 = new Investor('Investor1');
            $investmentDate = '03/10/2020';
            $investment = new Investment($investor1, $this->trancheA, '1000', $investmentDate);

            $this->loan->addLoanInvestment($investment);

            // Investor2
            $investor2 = new Investor('Investor2');
            $investmentDate = '04/10/2020';
            $this->investment2 = new Investment($investor2, $this->trancheA, '100', $investmentDate);

            $this->assertFalse($this->loan->doesLoanHasInvestment($this->investment2));
            $this->loan->addLoanInvestment($this->investment2);

        } catch (GeneralException $ex) {
            $this->assertSame('Investment on this Tranche is closed!', $ex->getMessage());
        }
    }

    /** @test */
    public function investor3_to_invest_500_in_trancheB()
    {
        // Investor3 investment

        $investor3 = new Investor('Investor3');
        $investmentDate = '10/10/2020';
        $investment = new Investment($investor3, $this->trancheB, '500', $investmentDate);

        $this->loan->addLoanInvestment($investment);

        $this->assertTrue($this->loan->doesLoanHasInvestment($investment));
    }

    /** @test */
    public function investor4_to_invest_1100_after_investor3_on_trancheB()
    {
        try {
            // Investor3 investment
            $investor3 = new Investor('Investor3');
            $investmentDate = '10/10/2020';
            $investment = new Investment($investor3, $this->trancheB, '500', $investmentDate);

            $this->loan->addLoanInvestment($investment);

            // Investor4 investment
            $investor4 = new Investor('Investor4');
            $investmentDate = '25/10/2020';
            $investment = new Investment($investor4, $this->trancheB, '1100', $investmentDate);

            $this->loan->addLoanInvestment($investment);

            $this->assertTrue($this->loan->doesLoanHasInvestment($investment));

        } catch(GeneralException $ex) {
            $this->assertSame('Insufficient balance to invest!', $ex->getMessage());
        }
    }

    /** @test */
    public function show_earnings_on_loan_investment()
    {
        $startDate = '01/10/2020';
        $endDate = '15/11/2020';
        $loan = new Loan($startDate, $endDate);

        $trancheA = new Tranche('Tranche A',3, 1000);
        $trancheB = new Tranche('Tranche B',6, 1000);
        $loan->addTranch($trancheA);
        $loan->addTranch($trancheB);

        $investor1 = new Investor('Investor1');
        $investmentDate = '03/10/2020';
        $investment1 = new Investment($investor1, $trancheA, '1000', $investmentDate);
        $loan->addLoanInvestment($investment1);

        $investor3 = new Investor('Investor3');
        $investmentDate = '10/10/2020';
        $investment3 = new Investment($investor3, $trancheB, '500', $investmentDate);
        $loan->addLoanInvestment($investment3);

        $loan->computeEarnings($investment1, '01/10/2020', '31/10/2020');
        $loan->computeEarnings($investment3, '01/10/2020', '31/10/2020');
        $investor1Earnings = collect($loan->getLoanInvestments())->first()->getTotalEarnings();
        $investor3Earnings = collect($loan->getLoanInvestments())->last()->getTotalEarnings();

        $this->assertEquals(28, $investor1Earnings);
        $this->assertEquals(21, $investor3Earnings);
    }
}
