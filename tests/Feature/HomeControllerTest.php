<?php

namespace Tests\Feature;

use App\Models\Investment;
use App\Models\Investor;
use App\Models\Tranche;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomeControllerTest extends TestCase
{

    public function test_index()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_index_data()
    {
        $trancheA = new Tranche('Tranche A',3, 1000);
        $trancheB = new Tranche('Tranche B',6, 1000);

        $investor1 = new Investor('Investor1');
        $investmentDate = '03/10/2020';
        $investment1 = new Investment($investor1, $trancheA, '1000', $investmentDate);

        $investor3 = new Investor('Investor3');
        $investmentDate = '10/10/2020';
        $investment3 = new Investment($investor3, $trancheB, '500', $investmentDate);

        $response = $this->get('/');
        $responseData = $response->getOriginalContent()->getData();
        $loan = $responseData['loan'];

        $this->assertTrue($loan->doesLoanHasInvestment($investment1));
        $this->assertTrue($loan->doesLoanHasInvestment($investment3));
    }

    /** @test */
    public function tranche_must_contain_interest_rate_in_the_response_data()
    {
        $trancheA = new Tranche('Tranche A',3, 1000);
        $trancheB = new Tranche('Tranche B',6, 1000);

        $investor1 = new Investor('Investor1');
        $investmentDate = '03/10/2020';
        $investment1 = new Investment($investor1, $trancheA, '1000', $investmentDate);

        $investor3 = new Investor('Investor3');
        $investmentDate = '10/10/2020';
        $investment3 = new Investment($investor3, $trancheB, '500', $investmentDate);

        $response = $this->get('/');
        $responseData = $response->getOriginalContent()->getData();
        $loan = $responseData['loan'];
        $tranches = $loan->getTranches();

        foreach($tranches as $tranche) {
            $this->assertTrue(is_float($tranche->getMonthlyInterestPercentage()));
        }
    }
}
