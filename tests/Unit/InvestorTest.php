<?php

namespace Tests\Unit;

use App\Models\Investor;
use PHPUnit\Framework\TestCase;

class InvestorTest extends TestCase
{
    /** @test */
    public function each_investor_must_start_with_1000()
    {
        $investor = new Investor('Investor1');
        $initialInvestment = '1000';
        $this->assertTrue($investor->getWalletBalance() === $initialInvestment);
    }
}
