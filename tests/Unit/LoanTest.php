<?php

namespace Tests\Unit;

use App\Exceptions\GeneralException;
use App\Models\Investment;
use App\Models\Investor;
use App\Models\Loan;
use App\Models\Tranche;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class LoanTest extends TestCase
{

    /** @test */
    public function loan_has_start_and_end_dates()
    {
        $this->assertClassHasAttribute("startDate", Loan::class);
        $this->assertClassHasAttribute("endDate", Loan::class);
    }


    /** @test */
    public function set_loan_start_and_end_dates()
    {
        $startDate = '01/10/2020';
        $endDate = '15/11/2020';

        $loan = new Loan($startDate, $endDate);

        $this->assertInstanceOf(Carbon::class,$loan->getStartDate());
        $this->assertInstanceOf(Carbon::class, $loan->getEndDate());
    }

    /** @test */
    public function does_tranche_exist_on_loan()
    {

        $startDate = '01/10/2020';
        $endDate = '15/11/2020';
        $loan = new Loan($startDate, $endDate);

        $trancheA = new Tranche('Tranche A',3, 1000);

        $loan->addTranch($trancheA);

        $this->assertTrue($loan->doesLoanHasTranche($trancheA));

    }

    /** @test */
    public function tranche_does_not_exist_on_loan()
    {

        $startDate = '01/10/2020';
        $endDate = '15/11/2020';
        $loan = new Loan($startDate, $endDate);

        $trancheA = new Tranche('Tranche A',3, 1000);

        $this->assertFalse($loan->doesLoanHasTranche($trancheA));

    }

    /** @test */
    public function loan_is_composed_of_2_tranches()
    {

        $startDate = '01/10/2020';
        $endDate = '15/11/2020';
        $loan = new Loan($startDate, $endDate);

        $trancheA = new Tranche('Tranche A',3, 1000);
        $trancheB = new Tranche('Tranche B',6, 1000);

        $loan->addTranch($trancheA);
        $loan->addTranch($trancheB);

        $this->assertTrue($loan->doesLoanHasTranche($trancheA));
        $this->assertTrue($loan->doesLoanHasTranche($trancheA));

    }


    /** @test */
    public function loan_must_be_composed_of_unique_tranches()
    {
        try {
            $startDate = '01/10/2020';
            $endDate = '15/11/2020';

            $tranchA = new Tranche('A', 3, 100);
            $loan = new Loan($startDate, $endDate);
            $loan->addTranch($tranchA);
            $loan->addTranch($tranchA);

        } catch (GeneralException $ex) {
            $this->assertSame('Tranche already exist', $ex->getMessage());
        }
    }

    public function test_if_date_carbonized()
    {
        $startDate = '01/10/2020';
        $endDate = '15/11/2020';
        $loan = new Loan($startDate, $endDate);

        $newDate = $loan->carbonizeDate($startDate);

        $this->assertInstanceOf(Carbon::class, $newDate);

    }

    /** @test */
    public function calculate_earnings_on_loan_investment()
    {
        $startDate = '01/10/2020';
        $endDate = '15/11/2020';
        $loan = new Loan($startDate, $endDate);

        $trancheA = new Tranche('Tranche A',3, 1000);
        $loan->addTranch($trancheA);

        $investor1 = new Investor('Investor1');
        $investmentDate = '03/10/2020';
        $investment = new Investment($investor1, $trancheA, '1000', $investmentDate);

        $loan->addLoanInvestment($investment);

        $earnings = $loan->computeEarnings($investment, '01/10/2020', '31/10/2020');

        $this->assertEquals(28, $earnings);
    }

    public function test_date_difference_calculation()
    {
        $startDate = '01/10/2020';
        $endDate = '31/10/2020';
        $loan = new Loan($startDate, $endDate);

        $startDate = $loan->carbonizeDate($startDate);
        $endDate = $loan->carbonizeDate($endDate);
        $diff = $endDate->diffInDays($startDate);

        $this->assertEquals(30, $diff);
    }
}
