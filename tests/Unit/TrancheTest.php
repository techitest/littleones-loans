<?php

namespace Tests\Unit;

use App\Models\Tranche;
use PHPUnit\Framework\TestCase;

class TrancheTest extends TestCase
{
    /** @test */
    public function convert_whole_numbers_into_percentage()
    {
        $number = 1;
        $tranche = new Tranche('A', $number, 100);
        $this->assertSame($number/100, $tranche->percentify($number));
    }

    /** @test */
    public function dont_convert_decimals_into_percentage()
    {
        $number = 0.01;
        $tranche = new Tranche('A', $number, 100);
        $this->assertSame($number, $tranche->percentify($number));
    }
}
