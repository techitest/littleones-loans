<?php


namespace App\Http\Controllers;


use App\Exceptions\GeneralException;
use App\Models\Investment;
use App\Models\Investor;
use App\Models\Loan;
use App\Models\Tranche;
use Illuminate\Support\Facades\Log;

class HomeController
{

    public function index()
    {
        $data = $this->processLoanInvestments();
        return view('welcome', [
            'loan' => $data,
            'currency' => Investment::CURRENCY
        ]);
    }

    public function processLoanInvestments()
    {

        try {
            $loan = $this->prepareLoan();

            $trancheA = $loan->getLoanTranche('Tranche A');
            $trancheB = $loan->getLoanTranche('Tranche B');

            $investor1 = new Investor('Investor1');
            $investmentDate = '03/10/2020';
            $investment = new Investment($investor1, $trancheA, '1000', $investmentDate);
            $loan->addLoanInvestment($investment);

            // fast-fowarding to 01/11/2020
            $loan->computeEarnings($investment, '01/10/2020', '31/10/2020');

            // Investor2
            $investor2 = new Investor('Investor2');
            $investmentDate = '04/10/2020';
            $investment2 = new Investment($investor2, $trancheA, '100', $investmentDate);

            $loan->addLoanInvestment($investment2);

        } catch(GeneralException $ex) {
            Log::info($ex->getMessage());
        }

        try {
            // Investor3 investment
            $investor3 = new Investor('Investor3');
            $investmentDate = '10/10/2020';
            $investment3 = new Investment($investor3, $trancheB, '500', $investmentDate);
            $loan->addLoanInvestment($investment3);

            // fast fowarding to 01/11/2020
            $loan->computeEarnings($investment3, '01/10/2020', '31/10/2020');

            // Investor4 investment
            $investor4 = new Investor('Investor4');
            $investmentDate = '25/10/2020';
            $investment4 = new Investment($investor4, $trancheB, '1100', $investmentDate);

            $loan->addLoanInvestment($investment4);

        }catch(GeneralException $ex) {
            Log::info($ex->getMessage());
        }

        return $loan;
    }

    public function prepareLoan()
    {
        $startDate = '01/10/2020';
        $endDate = '15/11/2020';

        $loan = new Loan($startDate, $endDate);
        $trancheA = new Tranche('Tranche A',3, 1000);
        $trancheB = new Tranche('Tranche B',6, 1000);

        $loan->addTranch($trancheA);
        $loan->addTranch($trancheB);

        return $loan;
    }

}