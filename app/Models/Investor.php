<?php


namespace App\Models;


class Investor
{

    /**
     * @var int
     */
    private $walletBalance = 1000;
    private $investments = [];
    /**
     * @var string
     */
    private $fullName;

    public function __construct(string $fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return int
     */
    public function getWalletBalance(): string
    {
        return $this->walletBalance;
    }

    /**
     * @param int $walletBalance
     */
    public function setWalletBalance(int $walletBalance): void
    {
        $this->walletBalance = (int)$walletBalance;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName(string $fullName): void
    {
        $this->fullName = $fullName;
    }

    public function storeInvestments($investment)
    {
        $this->investments[] = $investment;

    }

    public function getInvestments(Investment $investment)
    {
       return $this->investments;

    }

}