<?php


namespace App\Models;


use App\Exceptions\GeneralException;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class Loan
{
    protected $startDate;
    protected $endDate;
    private $loanTranches;
    private $loanInvestments = [];


    public function __construct(string $startDate, string $endDate)
    {
        $this->startDate = self::carbonizeDate($startDate);
        $this->endDate = self::carbonizeDate($endDate);
    }

    public static function carbonizeDate($date): Carbon
    {
        $dateData = explode('/', $date);
        return Carbon::create($dateData[2], $dateData[1], $dateData[0]);
    }

    public function addTranch(Tranche $newTranche)
    {
        if ($this->doesLoanHasTranche($newTranche)) {
            throw new GeneralException('Tranche already exist');
        }

        $this->loanTranches [] = $newTranche;
    }

    public function getLoanTranche(string $trancheName)
    {
        $tranches = collect($this->getTranches());

        // make sure the same tranche is not created multiple times
        foreach($tranches as $tranche) {
            if ($tranche->getTrancheName() === $trancheName) {
                return $tranche;
            }
        };

        return null;
    }

    public function doesLoanHasTranche(Tranche $newTranche)
    {
        $tranches = collect($this->getTranches());

        // make sure the same tranche is not created multiple times
        foreach($tranches as $tranche) {
            if ($tranche->getTrancheName() === $newTranche->getTrancheName()) {
                return true;
            }
        };

        return false;
    }

    public function doesLoanHasInvestment(Investment $newInvestment): bool
    {
        $investments = $this->getLoanInvestments();
        $results = [];

        foreach($investments as $investment) {
            $investor = $investment->getInvestor()->getFullName();
            $results[$investor][] = $investment->getInvestor()->getFullName() == $newInvestment->getInvestor()->getFullName();
            $results[$investor][] = $investment->getTranche()->getTrancheName() == $newInvestment->getTranche()->getTrancheName();
            $results[$investor][] = $investment->getAmount() == $newInvestment->getAmount();
        };

        $currentInvestor = $newInvestment->getInvestor()->getFullName();
        if (!array_key_exists($currentInvestor, $results)) return false;
        return in_array(false, $results[$currentInvestor], true) ? false : true;
    }

    public function getTranches()
    {
        return $this->loanTranches;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate): void
    {
        $this->startDate = self::carbonizeDate($startDate);
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }


    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate): void
    {
        $this->endDate = self::carbonizeDate($endDate);
    }


    public function isOpened(string $investmentDate)
    {
        if (Carbon::parse($investmentDate)->betweenIncluded($this->startDate, $this->endDate)) return true;

        return false;
    }

    /**
     * @return mixed
     */
    public function getLoanInvestments()
    {
        return $this->loanInvestments;
    }

    /**
     * @param mixed $loanInvestments
     */
    public function addLoanInvestment(Investment $loanInvestment): void
    {
        $investmentDate = $loanInvestment->getInvestmentDate();

        if (! $this->isOpened($investmentDate)) {
            throw new GeneralException('Loan is closed');
        }

        if (! $this->doesLoanHasTranche($loanInvestment->getTranche())) {
            throw new GeneralException('This Tranche does not exist on this Loan');
        }

        if ($loanInvestment->getTranche()->getMaximumInvestmentAmount() <= 0) {
            throw new GeneralException('Loan is no longer accepting investments');
        }

        $investor = $loanInvestment->getInvestor();
        $investmentAmount = $loanInvestment->getAmount();

        $walletBalance = $investor->getWalletBalance() - $investmentAmount;
        $investor->setWalletBalance($walletBalance);
        $investor->storeInvestments($loanInvestment);

        $investmentTranche = $loanInvestment->getTranche();
        $investmentTrancheBalance = $investmentTranche->getMaximumInvestmentAmount() - $investmentAmount;
        $investmentTranche->setMaximumInvestmentAmount($investmentTrancheBalance);

        $this->loanInvestments [] = $loanInvestment;
    }

    /**
     *
     * @param Investment $investment
     * @param string $from
     * @param string $to
     * @return float|int
     */
    public function computeEarnings(Investment $investment, string $from, string $to)
    {
        $periodStarts = self::carbonizeDate($from);
        $periodEnds = self::carbonizeDate($to);
        $monthlyInterestRate = $investment->getTranche()->getMonthlyInterestPercentage();

        $investmentPeriodInDays = $periodEnds->diffInDays($periodStarts);
        $dailyRate = $monthlyInterestRate / $investmentPeriodInDays;
        $noOfDaysInvested = $periodEnds->diffInDays($investment->getInvestmentDate());

        $earnings = $dailyRate * $investment->getAmount() * $noOfDaysInvested;

        $investment->setEarnings([
            'period' => "$from - $to",
            'amount' => $earnings
        ]);

        $investment->setTotalEarnings($earnings);
        return $earnings;
    }

}