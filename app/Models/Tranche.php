<?php


namespace App\Models;


use phpDocumentor\Reflection\Types\Integer;

class Tranche
{

    const CURRENCY = '£';

    /**
     * @var int
     */
    private $monthlyInterestPercentage;
    /**
     * @var int
     */
    private $maximumInvestmentAmount;
    /**
     * @var string
     */
    private $trancheName;

    public function __construct(string $trancheName, int $monthlyInterestPercentage, int $maximumInvestmentAmount)
    {
        $this->monthlyInterestPercentage = $this->percentify($monthlyInterestPercentage);
        $this->maximumInvestmentAmount = $maximumInvestmentAmount;
        $this->trancheName = $trancheName;
    }

    public function percentify($value)
    {
        if ($value >= 1) {
            return $value / 100;
        }

        return $value;
    }

    /**
     * @return int
     */
    public function getMonthlyInterestPercentage()
    {
        return $this->monthlyInterestPercentage;
    }

    /**
     * @param int $monthlyInterestPercentage
     */
    public function setMonthlyInterestPercentage(int $monthlyInterestPercentage): void
    {
        $this->monthlyInterestPercentage = $monthlyInterestPercentage;
    }

    /**
     * @return string
     */
    public function getTrancheName(): string
    {
        return $this->trancheName;
    }

    /**
     * @param string $trancheName
     */
    public function setTrancheName(string $trancheName): void
    {
        $this->trancheName = $trancheName;
    }

    /**
     * @return int
     */
    public function getMaximumInvestmentAmount()
    {
        return $this->maximumInvestmentAmount;
    }

    /**
     * @param int $maximumInvestmentAmount
     */
    public function setMaximumInvestmentAmount(int $maximumInvestmentAmount): void
    {
        $this->maximumInvestmentAmount = $maximumInvestmentAmount;
    }

//    public function saveAccruedEarnings()
//    {
//
//    }
}