<?php


namespace App\Models;


use App\Exceptions\GeneralException;
use Carbon\Carbon;

class Investment
{

    const CURRENCY = '£';

    /**
     * @var Investor
     */
    private $investor;
    /**
     * @var Tranche
     */
    private $tranche;
    /**
     * @var string
     */
    private $investmentDate;
    /**
     * @var int
     */
    private $amount;

    private $earnings = [];
    private $totalEarnings;

    public function __construct(Investor $investor, Tranche $tranche, int $amount, string $investmentDate)
    {
        $this->investor = $investor;
        $this->tranche = $tranche;
        $this->amount = $amount;

        if (! self::doesInvestorHasEnoughBalance($investor, $amount)) {
            throw new GeneralException('Insufficient balance to invest!');
        }

        if (! self::doesTranchHasEnoughInvestmentAmount($tranche, $amount)) {
            throw new GeneralException('Investment on this Tranche is closed!');
        }

        $dateData = explode('/', $investmentDate);
        $this->investmentDate = Carbon::create($dateData[2], $dateData[1], $dateData[0]);
    }

    public static function doesInvestorHasEnoughBalance(Investor $investor, $investmentAmount)
    {
        return $investor->getWalletBalance() >= $investmentAmount ?? false;
    }

    public static function doesTranchHasEnoughInvestmentAmount(Tranche $tranche, $investmentAmount)
    {
        return $tranche->getMaximumInvestmentAmount() >= $investmentAmount ?? false;
    }

    /**
     * @return Investor
     */
    public function getInvestor(): Investor
    {
        return $this->investor;
    }

    /**
     * @param Investor $investor
     */
    public function setInvestor(Investor $investor): void
    {
        $this->investor = $investor;
    }

    /**
     * @return Tranche
     */
    public function getTranche(): Tranche
    {
        return $this->tranche;
    }

    /**
     * @param Tranche $tranche
     */
    public function setTranche(Tranche $tranche): void
    {
        $this->tranche = $tranche;
    }

    /**
     * @return string
     */
    public function setInvestmentDate(string $investmentDate): string
    {
        $dateData = explode('/', $investmentDate);
        $this->investmentDate = Carbon::create($dateData[2], $dateData[1], $dateData[0]);
    }

    /**
     * @return string
     */
    public function getInvestmentDate(): string
    {
        return $this->investmentDate;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return array
     */
    public function getEarnings(): array
    {
        return $this->earnings;
    }

    /**
     * @param array $earnings
     */
    public function setEarnings(array $earnings): void
    {
        $this->earnings[] = $earnings;
    }

    /**
     * @return mixed
     */
    public function getTotalEarnings()
    {
        return $this->totalEarnings;
    }

    /**
     * @param mixed $totalEarnings
     */
    public function setTotalEarnings($periodEarnings): void
    {
        $this->totalEarnings = $this->totalEarnings + $periodEarnings;
    }
}