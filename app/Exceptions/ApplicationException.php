<?php

namespace App\Exceptions;

use Exception;

class ApplicationException extends Exception
{
    private $context;

    public function __construct(string $message, array $context, $code = 0, Exception $previous = null)
    {
        $this->context = $context;

        parent::__construct($message, $code, $previous);
    }

    public function getContext(): array
    {
        return $this->context;
    }
}
